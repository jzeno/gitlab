# frozen_string_literal: true

require 'spec_helper'

RSpec.describe AuditEvents::StreamDestinationSyncHelper, feature_category: :audit_events do
  let(:helper) { Class.new { include AuditEvents::StreamDestinationSyncHelper }.new }

  describe '#create_legacy_destination' do
    context 'when feature flag is enabled' do
      before do
        stub_feature_flags(audit_events_external_destination_streamer_consolidation_refactor: true)
      end

      describe 'http destinations' do
        context 'when instance level' do
          let!(:source) do
            create(:audit_events_instance_external_streaming_destination, :http) # rubocop:disable RSpec/FactoryBot/AvoidCreate -- need to persist for tests
          end

          let!(:event_type_filter) do
            create(:audit_events_instance_event_type_filters, # rubocop:disable RSpec/FactoryBot/AvoidCreate -- need to persist for tests
              external_streaming_destination: source,
              audit_event_type: 'user_created')
          end

          let!(:namespace_filter) do
            create(:audit_events_streaming_instance_namespace_filters, # rubocop:disable RSpec/FactoryBot/AvoidCreate -- need to persist for tests
              external_streaming_destination: source)
          end

          it 'creates legacy destination with all associated records' do
            destination = helper.create_legacy_destination(source)

            aggregate_failures do
              expect(destination).to be_a(AuditEvents::InstanceExternalAuditEventDestination)
              expect(destination.name).to eq(source.name)
              expect(destination.destination_url).to eq(source.config['url'])
              expect(destination.verification_token).to eq(source.secret_token)
              expect(destination.stream_destination_id).to eq(source.id)
              expect(source.legacy_destination_ref).to eq(destination.id)

              expect(destination.event_type_filters.count).to eq(1)
              expect(destination.event_type_filters.first.audit_event_type).to eq('user_created')

              expect(destination.namespace_filter&.namespace)
                .to eq(namespace_filter.namespace)
            end
          end
        end

        context 'when group level' do
          let(:group) { create(:group) } # rubocop:disable RSpec/FactoryBot/AvoidCreate -- need to persist for tests

          let!(:source) do
            create(:audit_events_group_external_streaming_destination, :http, group: group) # rubocop:disable RSpec/FactoryBot/AvoidCreate -- need to persist for tests
          end

          let!(:event_type_filter) do
            create(:audit_events_group_event_type_filters, # rubocop:disable RSpec/FactoryBot/AvoidCreate -- need to persist for tests
              external_streaming_destination: source,
              namespace: group,
              audit_event_type: 'user_created')
          end

          let!(:namespace_filter) do
            create(:audit_events_streaming_group_namespace_filters, # rubocop:disable RSpec/FactoryBot/AvoidCreate -- need to persist for tests
              external_streaming_destination: source,
              namespace: group)
          end

          it 'creates legacy destination with all associated records' do
            destination = helper.create_legacy_destination(source)

            aggregate_failures do
              expect(destination).to be_a(AuditEvents::ExternalAuditEventDestination)
              expect(destination.name).to eq(source.name)
              expect(destination.namespace_id).to eq(group.id)
              expect(destination.destination_url).to eq(source.config['url'])
              expect(destination.verification_token).to eq(source.secret_token)
              expect(destination.stream_destination_id).to eq(source.id)
              expect(source.legacy_destination_ref).to eq(destination.id)
              expect(destination.event_type_filters.count).to eq(1)
              expect(destination.event_type_filters.first.audit_event_type).to eq('user_created')
              expect(destination.event_type_filters.first.group_id).to eq(group.id)
              expect(destination.namespace_filter&.namespace).to eq(group)
            end
          end
        end
      end

      describe 'aws destinations' do
        context 'when instance level' do
          let!(:source) do
            create(:audit_events_instance_external_streaming_destination, :aws) # rubocop:disable RSpec/FactoryBot/AvoidCreate -- need to persist for tests
          end

          it 'creates legacy destination correctly' do
            destination = helper.create_legacy_destination(source)

            aggregate_failures do
              expect(destination).to be_a(AuditEvents::Instance::AmazonS3Configuration)
              expect(destination.name).to eq(source.name)
              expect(destination.bucket_name).to eq(source.config['bucketName'])
              expect(destination.aws_region).to eq(source.config['awsRegion'])
              expect(destination.access_key_xid).to eq(source.config['accessKeyXid'])
              expect(destination.secret_access_key).to eq(source.secret_token)
              expect(destination.stream_destination_id).to eq(source.id)
              expect(source.legacy_destination_ref).to eq(destination.id)
            end
          end
        end

        context 'when group level' do
          let(:group) { create(:group) } # rubocop:disable RSpec/FactoryBot/AvoidCreate -- need to persist for tests

          let!(:source) do
            create(:audit_events_group_external_streaming_destination, :aws, group: group) # rubocop:disable RSpec/FactoryBot/AvoidCreate -- need to persist for tests
          end

          it 'creates legacy destination correctly' do
            destination = helper.create_legacy_destination(source)

            aggregate_failures do
              expect(destination).to be_a(AuditEvents::AmazonS3Configuration)
              expect(destination.name).to eq(source.name)
              expect(destination.namespace_id).to eq(group.id)
              expect(destination.bucket_name).to eq(source.config['bucketName'])
              expect(destination.aws_region).to eq(source.config['awsRegion'])
              expect(destination.access_key_xid).to eq(source.config['accessKeyXid'])
              expect(destination.secret_access_key).to eq(source.secret_token)
              expect(destination.stream_destination_id).to eq(source.id)
              expect(source.legacy_destination_ref).to eq(destination.id)
            end
          end
        end
      end

      describe 'gcp destinations' do
        context 'when instance level' do
          let!(:source) do
            create(:audit_events_instance_external_streaming_destination, :gcp) # rubocop:disable RSpec/FactoryBot/AvoidCreate -- need to persist for tests
          end

          it 'creates legacy destination correctly' do
            destination = helper.create_legacy_destination(source)

            aggregate_failures do
              expect(destination).to be_a(AuditEvents::Instance::GoogleCloudLoggingConfiguration)
              expect(destination.name).to eq(source.name)
              expect(destination.google_project_id_name).to eq(source.config['googleProjectIdName'])
              expect(destination.log_id_name).to eq(source.config['logIdName'])
              expect(destination.client_email).to eq(source.config['clientEmail'])
              expect(destination.private_key).to eq(source.secret_token)
              expect(destination.stream_destination_id).to eq(source.id)
              expect(source.legacy_destination_ref).to eq(destination.id)
            end
          end
        end

        context 'when group level' do
          let(:group) { create(:group) } # rubocop:disable RSpec/FactoryBot/AvoidCreate -- need to persist for tests

          let!(:source) do
            create(:audit_events_group_external_streaming_destination, :gcp, group: group) # rubocop:disable RSpec/FactoryBot/AvoidCreate -- need to persist for tests
          end

          it 'creates legacy destination correctly' do
            destination = helper.create_legacy_destination(source)

            aggregate_failures do
              expect(destination).to be_a(AuditEvents::GoogleCloudLoggingConfiguration)
              expect(destination.name).to eq(source.name)
              expect(destination.google_project_id_name).to eq(source.config['googleProjectIdName'])
              expect(destination.log_id_name).to eq(source.config['logIdName'])
              expect(destination.client_email).to eq(source.config['clientEmail'])
              expect(destination.private_key).to eq(source.secret_token)
              expect(destination.stream_destination_id).to eq(source.id)
              expect(source.legacy_destination_ref).to eq(destination.id)
            end
          end
        end
      end

      context 'when an error occurs during creation' do
        let(:group) { create(:group) } # rubocop:disable RSpec/FactoryBot/AvoidCreate -- need to persist for tests
        let(:source) do
          create(:audit_events_group_external_streaming_destination, :http, group: group) # rubocop:disable RSpec/FactoryBot/AvoidCreate -- need to persist for tests
        end

        let(:mock_destination) { build(:external_audit_event_destination, group: group) }

        before do
          allow(AuditEvents::ExternalAuditEventDestination)
            .to receive(:new)
            .and_return(mock_destination)

          allow(mock_destination)
            .to receive(:save!)
            .and_raise(described_class::CreateError, 'Test error')
        end

        it 'returns nil and tracks the error' do
          expect(Gitlab::ErrorTracking)
            .to receive(:track_exception)
            .with(
              an_instance_of(described_class::CreateError),
              audit_event_destination_model: source.class.name
            )

          expect(helper.create_legacy_destination(source)).to be_nil
        end
      end
    end

    context 'when feature flag is disabled' do
      before do
        stub_feature_flags(audit_events_external_destination_streamer_consolidation_refactor: false)
      end

      let!(:source) do
        create(:audit_events_instance_external_streaming_destination) # rubocop:disable RSpec/FactoryBot/AvoidCreate -- need to persist for tests
      end

      it 'returns nil' do
        expect(helper.create_legacy_destination(source)).to be_nil
      end
    end
  end
end
